﻿
using NotifyDatabaseService.service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NotifyDatabaseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DatabaseService" in both code and config file together.
    public class DatabaseService : IDatabaseService
    {
        private Database db = new Database();
        public List<User> Users = new List<User>();

        public User getUserByPNo(string pNo)
        {
            User user = new User();
            SqlDataReader data = db.GetUserByPNo(pNo);
            if (data.Read())
            {
                user.Id = Convert.ToInt32(data["id"]);
                user.Name = data["name"].ToString();
                user.Pass = data["pass"].ToString();
                user.PNo = data["phone_No"].ToString();
                user.Type = (UserType)Convert.ToInt32(data["type"]);
                data.Close();
                return user;
            }
            else
            {
                return user = null;
            }
        }

        public User getUserByUserName(string userName)
        {
            User user = new User();
            SqlDataReader data = db.GetUserByUserName(userName);
            if (data.Read())
            {
                user.Id = Convert.ToInt32(data["id"]);
                user.Name = data["name"].ToString();
                user.Pass = data["pass"].ToString();
                user.PNo = data["phone_No"].ToString();
                user.Type = (UserType)Convert.ToInt32(data["type"]);
                data.Close();
                return user;
            }
            else
            {
                return user = null;
            }
        }

        public List<Group> getGroupsByUserId(int id)
        {
            List<Group> groups = new List<Group>();
            DataSet ds = db.getGroupsByUserId(id);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Group group = new Group { Id = Convert.ToInt32(row["group_id"]), At_name = row["at_name"].ToString(), GroupName = row["group_name"].ToString() };
                    groups.Add(group);
                }
            }
            return groups;
        }

        public int saveMessage(int senderId, int groupId, Message msg)
        {
            Console.WriteLine("Sender id: "+senderId+ " groupId "+ groupId + msg.Content);
            int msgid = db.SaveMessage(senderId, groupId, msg);
            Console.WriteLine(msgid);
            Users = getUsersOfGroup(groupId);
            foreach(User user in Users)
            {
                sendMessage(user.Id, msgid , DeliveryStatus.NotDelivered);
            }
            return msgid;
        }

        public void sendMessage(int receiverId, int msgId, DeliveryStatus status)
        {
            db.sendMessage(receiverId, msgId, status);
        }

        public void deliverMessage(int receiverId, int msgId, DeliveryStatus status)
        {
            db.changeMsgStatus(receiverId, msgId, status);
        }

        public void updateUser(string userId, User user)
        {
            throw new NotImplementedException();
        }

        public void deleteUser(string userId)
        {
            throw new NotImplementedException();
        }

        public void createGroup(Group group)
        {
            throw new NotImplementedException();
        }

        public void createUser(User user)
        {
            throw new NotImplementedException();
        }

        public void assignUserToGroup(List<int> userIds, int groupId)
        {
            throw new NotImplementedException();
        }

        public void unassignUserFromGroup(List<int> userIds, int groupId)
        {
            throw new NotImplementedException();
        }

        public void updateGroup(int groupId, Group group)
        {
            throw new NotImplementedException();
        }

        public void deleteGroup(int groupId)
        {
            throw new NotImplementedException();
        }

        public List<User> getUsersOfGroup(int groupId)
        {
            List<User> users = new List<User>();
            DataSet ds = db.getUsersOfGroup(groupId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    User user = new User { Id = Convert.ToInt32(row["id"]), Name = row["name"].ToString(), PNo = row["phone_No"].ToString(), Type = (UserType)Convert.ToInt32(row["type"])
                    };
                    users.Add(user);
                }
            }
            return users;
        }

        public List<Message> getMessagesOfUser(int userId)
        {
            List<Message> msgs = new List<Message>();
            DataSet ds = db.getAllMessages(userId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Group group = getGroupByGroupid(Convert.ToInt32(row["group_id"]));
                    Message msg = new Message { ID = Convert.ToInt32(row["msg_id"]), Content = row["msg_content"].ToString(), GroupName = group.GroupName };
                    msgs.Add(msg);
                }
            }
            return msgs;
        }

        public Group getGroupByAtName(string atName)
        {
            Group group = new Group();
            SqlDataReader data = db.getGroupByAtName(atName);
            if (data.Read())
            {
                group.Id = Convert.ToInt32(data["id"]);
                group.GroupName = data["name"].ToString();
                group.At_name = data["at_name"].ToString();
                data.Close();
                return group;
            }
            else
            {
                return group = null;
            }
        }


        public User testMethod(string atName)
        {
            Message m = new Message();
            m.Content = "kfasdljf";

            User us = new User();
            us.Id = 11;
            us.Name = "testName";
            us.UserName = "testUser";
            us.Pass = "123456";
            us.PNo = "11111";
            us.Type = UserType.Admin;
            return us;
        }

        public Group getGroupByGroupName(string groupName)
        {
            Group group = new Group();
            SqlDataReader data = db.getGroupByGroupName(groupName);
            if (data.Read())
            {
                group.Id = Convert.ToInt32(data["id"]);
                group.GroupName = data["name"].ToString();
                group.At_name = data["at_name"].ToString();
                data.Close();
                return group;
            }
            else
            {
                return group = null;
            }
        }

        public int getPhoneNumberByUserId(int userId)
        {
            int phoneNumber = db.getPhoneNumberByUserId(userId);
            return phoneNumber;
        }

        public List<Message> getFewMessages(int userId)
        {
            Console.WriteLine("Recive to Service");
            List<Message> msgs = new List<Message>();
            DataSet ds = db.getFewMessages(userId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Group group = getGroupByGroupid(Convert.ToInt32(row["group_id"]));
                    Message msg = new Message { ID = Convert.ToInt32(row["msg_id"]), Content = row["msg_content"].ToString(), GroupName = group.GroupName };
                    msgs.Add(msg);
                    Console.WriteLine(group.GroupName);
                }
            }
            return msgs;
        }

        public List<Message> getSendMessagesBySenderId(int userId)
        {
            List<Message> msgs = new List<Message>();
            DataSet ds = db.getSendMessageBuSenderId(userId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Message msg = new Message { ID = Convert.ToInt32(row["msg_id"]), Content = row["msg_content"].ToString(), GroupName = row["group_name"].ToString() };
                    msgs.Add(msg);
                }
            }
            return msgs;
        }

        public User getUserByUserId(int userId)
        {
            User user = new User();
            SqlDataReader data = db.GetUserByUserId(userId);
            if (data.Read())
            {
                user.Id = Convert.ToInt32(data["id"]);
                user.Name = data["name"].ToString();
                user.Pass = data["pass"].ToString();
                user.PNo = data["phone_No"].ToString();
                user.Type = (UserType)Convert.ToInt32(data["type"]);
                data.Close();
                return user;
            }
            else
            {
                return user = null;
            }
        }

        public Group getGroupByGroupid(int groupId)
        {
            Group group = new Group();
            SqlDataReader data = db.getGroupByGroupId(groupId);
            if (data.Read())
            {
                group.Id = Convert.ToInt32(data["group_id"]);
                group.GroupName = data["group_name"].ToString();
                group.At_name = data["at_name"].ToString();
                data.Close();
                return group;
            }
            else
            {
                return group = null;
            }
        }
    }
}
