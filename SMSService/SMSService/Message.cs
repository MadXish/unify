﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSService
{
    public class Message
    {
        private string pNumber;
        private string textContent;
        private string groupName;

        public string GroupName
        {
            get { return groupName; }
            set { groupName = value; }
        }

        public string PNumber
        {
            get{ return pNumber; }
            set{ pNumber = value; }
        }

        public string TextContent
        {
            get{ return textContent; }
            set{ textContent = value; }
        }
    }

    public enum DilivaryStatus
    {
        Delivered = 1,
        NotDelivered = 2
    }
}
