﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UniFyWeb
{
    public partial class StudentUserProfile : System.Web.UI.Page
    {
        UniFyDBService.DatabaseServiceClient client = new UniFyDBService.DatabaseServiceClient();
        public List<UniFyDBService.Message> Messages = new List<UniFyDBService.Message>();
        UniFyDBService.User newUser = new UniFyDBService.User();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bringId();
            }

            if (Session["new"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                PanelStudProf.Visible = true;
                PanelEditProfileStud.Visible = false;

                newUser = (UniFyDBService.User)Session["new"];
                Messages = client.getMessagesOfUser(newUser.Id);

                TableHeaderRow hrow = new TableHeaderRow();
                TableCell cellh1 = new TableCell();
                cellh1.Text = "Message";
                cellh1.Style["color"] = "black";
                cellh1.Style["font-size"] = "medium";
                cellh1.Style["font-weight"] = "bold";
                cellh1.Attributes.Add("class", "warning");
                hrow.Cells.Add(cellh1);
                msgTable.Rows.Add(hrow);

                foreach (UniFyDBService.Message msg in Messages)
                {
                    TableRow row = new TableRow();
                    TableCell cell1 = new TableCell();

                    cell1.Text = msg.Content;
                    cell1.Style["color"] = "black";
                    cell1.Attributes.Add("class", "warning");

                    row.Cells.Add(cell1);
                    msgTable.Rows.Add(row);
                }
            }
        }
   
        private void bringId()
        {
            newUser = (UniFyDBService.User)Session["new"];
            lblFName.Text = newUser.Name.ToString();
            lblTel.Text = newUser.PNo.ToString();
            lblAddress.Text = "Colombo";
            lblDOB.Text = "1992/10/11/";
            lblGender.Text = "Male";
            lblEmail.Text = "zone@gmail.com";
        }

        protected void LinkBtnProfile_Click(object sender, EventArgs e)
        {
            PanelEditProfileStud.Visible = false;
            PanelStudProf.Visible = true;
        }

        protected void LinkBtnEditProfile_Click(object sender, EventArgs e)
        {
            PanelEditProfileStud.Visible = true;
            PanelStudProf.Visible = false;
        }
    }
}