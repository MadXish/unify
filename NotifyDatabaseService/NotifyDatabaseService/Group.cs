﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotifyDatabaseService
{
    public class Group
    {
        private int id;
        private string groupName;
        private string at_name;

        public int Id { get; set; }

        public string GroupName { get; set; }

        public string At_name { get; set; }
    }
}
