﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotifyDatabaseService.service
{
    class Database
    {
        private string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        public SqlDataReader GetUserByUserName(string userName)
        {
            SqlDataReader data;
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetUserByUserName", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramUserName = new SqlParameter("@userName", userName);
            cmd.Parameters.Add(paramUserName);
            cn.Open();
            data = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return data;
        }

        public SqlDataReader GetUserByPNo(string pNo)
        {
            SqlDataReader data;
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetUserByPNo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramPNo = new SqlParameter("@pNo", pNo);
            cmd.Parameters.Add(paramPNo);
            cn.Open();
            data = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return data;
        }

        public SqlDataReader GetUserByUserId(int userId)
        {
            SqlDataReader data;
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetUserById", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramPNo = new SqlParameter("@id", userId);
            cmd.Parameters.Add(paramPNo);
            cn.Open();
            data = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return data;
        }

        public DataSet getGroupsByUserId(int userId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetGroupsByUserId", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@userid", userId);
            cmd.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public int SaveMessage(int sender, int groupId, Message msg)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spSaveMsg", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramContent = new SqlParameter("@msgContent", msg.Content);
            cmd.Parameters.Add(paramContent);
            SqlParameter paramSender = new SqlParameter("@senderId", sender);
            cmd.Parameters.Add(paramSender);
            SqlParameter paramGroupId = new SqlParameter("@groupId", groupId);
            cmd.Parameters.Add(paramGroupId);
            cn.Open();
            int id = (int)cmd.ExecuteScalar();
            cn.Close();
            return id;
        }

        public void sendMessage(int receiverId, int msgId, DeliveryStatus status)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spSendMsg", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramReceiverId = new SqlParameter("@receiverId", receiverId);
            cmd.Parameters.Add(paramReceiverId);
            SqlParameter paramMsgId = new SqlParameter("@msgId", msgId);
            cmd.Parameters.Add(paramMsgId);
            SqlParameter paramStatus = new SqlParameter("@deliveryStatus", status);
            cmd.Parameters.Add(paramStatus);
            cn.Open();
            //int id = (int)cmd.ExecuteScalar();
            cmd.ExecuteScalar();
            cn.Close();
        }

        public void changeMsgStatus(int receiverId, int msgId, DeliveryStatus status)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spChangeMsgStatus", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramReceiverId = new SqlParameter("@receiverId", receiverId);
            cmd.Parameters.Add(paramReceiverId);
            SqlParameter paramMsgId = new SqlParameter("@msgId", msgId);
            cmd.Parameters.Add(paramMsgId);
            SqlParameter paramStatus = new SqlParameter("@deliveryStatus", status);
            cmd.Parameters.Add(paramStatus);
            cn.Open();
            int id = (int)cmd.ExecuteScalar();
            cn.Close();
        }
        public DataSet getUsersOfGroup(int userId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetUsersOfGroup", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@groupId", userId);
            cmd.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmd;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public SqlDataReader getGroupByAtName(string atName)
        {
            SqlDataReader data;
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetGroupByAtName", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramAtName = new SqlParameter("@atName", atName);
            cmd.Parameters.Add(paramAtName);
            cn.Open();
            data = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return data;
        }

        public SqlDataReader getGroupByGroupId(int groupId)
        {
            SqlDataReader data;
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("[spGetGroupByGroupId]", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramAtName = new SqlParameter("@group_id", groupId);
            cmd.Parameters.Add(paramAtName);
            cn.Open();
            data = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return data;
        }

        public DataSet getAllMessages(int reciverId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmb = new SqlCommand("spGetAllMessages", cn);
            cmb.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@receiver_id", reciverId);
            cmb.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmb;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public SqlDataReader getGroupByGroupName(string groupName)
        {
            SqlDataReader data;
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetGroupByGroupName", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramGroupName = new SqlParameter("@group_name", groupName);
            cmd.Parameters.Add(paramGroupName);
            cn.Open();
            data = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return data;
        }

        public int getPhoneNumberByUserId(int userId)
        {
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("spGetPhoneByUserId", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramUserId = new SqlParameter("@user_id", userId);
            cmd.Parameters.Add(paramUserId);
            cn.Open();
            int number = (int)cmd.ExecuteScalar();
            cn.Close();
            return number;
        }

        public DataSet getFewMessages(int userId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmb = new SqlCommand("spGetFewResaults", cn);
            cmb.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@reciver_id", userId);
            cmb.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmb;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

        public DataSet getSendMessageBuSenderId(int userId)
        {
            SqlDataAdapter adt = new SqlDataAdapter();
            DataSet ds = new DataSet();
            SqlConnection cn = new SqlConnection(cs);
            SqlCommand cmb = new SqlCommand("spGetSendMessagesbySenderId", cn);
            cmb.CommandType = CommandType.StoredProcedure;
            SqlParameter pramId = new SqlParameter("@sender_Id", userId);
            cmb.Parameters.Add(pramId);
            cn.Open();
            adt.SelectCommand = cmb;
            adt.Fill(ds);
            cn.Close();
            return ds;
        }

    }
}
