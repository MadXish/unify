﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotifyDatabaseService
{
    public class User
    {
        private int id;
        private string name;
        private string userName;
        private string pNo;
        private string pass;
        private UserType type;

        public int Id 
        { 
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string UserName
        {
            get 
            { 
                return userName; 
            }
            set 
            { 
                userName = value; 
            }
        }

        public string PNo
        {
            get 
            { 
                return pNo; 
            }
            set 
            { 
                pNo = value; 
            }
        }

        public string Pass
        {
            get 
            { 
                return pass; 
            }
            set 
            { 
                pass = value; 
            }
        }

        public UserType Type
        {
            get 
            { 
                return type; 
            }
            set 
            { 
                type = value; 
            }
        }
    }

    public enum UserType
    {
        Lecturer = 1,
        Student = 2,
        Admin = 3
    }
}
