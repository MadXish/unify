﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using DatabaseServiceConsumer.DatabaseService;

namespace SMSService
{
    public class SMSService : ISMSService
    {
        SerialPort port = new SerialPort();
        clsSMS objclsSMS = new clsSMS();

        public void portOpen()
        {
            try
            {
                string[] ports = SerialPort.GetPortNames();
                Console.WriteLine(ports);
                this.port = objclsSMS.OpenPort("COM4", 9600, 8, 300, 300);
            }
            catch
            {
                Console.WriteLine("Error on opernig ports....!");
            }
        }

        public void sendMsg(Message msg)
        {
            try
            {
                portOpen();
                objclsSMS.sendMsg(this.port, msg.PNumber, msg.TextContent);
                objclsSMS.ClosePort(this.port);
            }
            catch (Exception)
            {
                Console.WriteLine("Error in message sending.....!");
            }
        }

        public void sendMessages(Message msg)
        {
            sendMsg(msg);
        }

        public void reciveSMS()
        {
            //objclsSMS.reciveMsg();
        }


        public void deliverMesssage(int id, string message)
        {
            try
            {
                DatabaseServiceClient client = new DatabaseServiceClient("BasicHTTP");
                DatabaseServiceConsumer.DatabaseService.User user = client.getUserByUserId(id);
                portOpen();
                string newPhoneNumer = user.PNo.Remove(0, 1);
                Console.WriteLine(newPhoneNumer);
                string realPhoneNumber = "+94" + newPhoneNumer;
                Console.WriteLine(realPhoneNumber);
                objclsSMS.sendMsg(this.port, realPhoneNumber, message);
                objclsSMS.ClosePort(this.port);
            }
            catch (Exception)
            {
                Console.WriteLine("Error in message sending.....!");
            }
        }


        public void deliverMsgFromAndroidService(List<int> userIDs, DatabaseServiceConsumer.DatabaseService.Message message)
        {
            foreach(int userID in userIDs)
            {
                DatabaseServiceClient client = new DatabaseServiceClient("BasicHTTP");
                DatabaseServiceConsumer.DatabaseService.User user = client.getUserByUserId(userID);
                portOpen();
                string newPhoneNumer = user.PNo.Remove(0, 1);
                Console.WriteLine("android " + newPhoneNumer);
                string realPhoneNumber = "+94" + newPhoneNumer;
                Console.WriteLine("android " + realPhoneNumber);
                objclsSMS.sendMsg(this.port, realPhoneNumber, message.Content );
                objclsSMS.ClosePort(this.port);
            }
        }
    }
}
