﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin_Dashboard.aspx.cs" Inherits="UniFyWeb.Admin_Dashboard" MasterPageFile="~/Index.Master"%>

<asp:Content ID="contentAdmin" ContentPlaceHolderID="contentHolder" runat="server">
    <div id="wrapper">
        <!------Sidebar------>
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">Admin Panal
                    </a>
                </li>
                <li>
                    <asp:LinkButton ID="LinkButtonAddGroup" runat="server" OnClick="LinkButtonAddGroup_Click">Manage Group</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="LinkButtonAddLecture" runat="server" OnClick="LinkButtonAddLecture_Click">Manage Lecture</asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="LinkButtonAddStudents" runat="server" OnClick="LinkButtonAddStudents_Click">Manage Student</asp:LinkButton>
                </li>
            </ul>
        </div>
        <!------End of Sidebar------>

        <!----------Add Group Content---------->
        <asp:Panel ID="PanelAddGroup" runat="server">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="registrationform">
                    <div class="form-horizontal">
                        <fieldset>
                            <legend>Manage Groups <i class="fa fa-pencil pull-right"></i></legend>
                            <table class="table" style="color: black">
                                <tr>
                                    <th class="warning">Software 13.1</th>
                                    <th class="success col-lg-2">Delete</th>
                                </tr>
                                <tr>
                                    <td class="warning">Netword 14.1</td>
                                    <td class="success col-lg-2">
                                        <asp:Button ID="Button1" runat="server" CssClass="btn-danger" Text="Delete" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="warning">MIS 12.2</td>
                                    <td class="success col-lg-2">
                                        <asp:Button ID="Button2" runat="server" CssClass="btn-danger" Text="Delete" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="registrationform">
                    <div class="form-horizontal">
                        <fieldset>
                            <legend>Add Groups <i class="fa fa-pencil pull-right"></i></legend>

                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="Group Name" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox5" runat="server" placeholder="Group Name" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Year" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox6" runat="server" placeholder="Year" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="Batch NO" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox1" runat="server" placeholder="Batch NO" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Add" OnClick="btnSubmit_Click" />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-warning" Text="Cancel" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>

        </asp:Panel>
        <!----------End of Add Group Content---------->

        <!-----------SignUp Lecture Content----------->
        <asp:Panel ID="PanelSignUpLec" runat="server">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="registrationform">
                    <div class="form-horizontal">
                        <fieldset>
                            <legend>Manage Groups <i class="fa fa-pencil pull-right"></i></legend>
                            <table class="table" style="color: black">
                                <tr>
                                    <th class="warning">Mr.Kumara Prasad</th>
                                    <th class="success col-lg-2">Delete</th>
                                </tr>
                                <tr>
                                    <td class="warning">Mr. jhon creg</td>
                                    <td class="success col-lg-2">
                                        <asp:Button ID="Button10" runat="server" CssClass="btn-danger" Text="Delete" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="registrationform">
                    <div class="form-horizontal">
                        <fieldset>
                            <legend>Lecture Registration <i class="fa fa-pencil pull-right"></i></legend>

                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Full Name" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox2" runat="server" placeholder="Full Name" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="ID" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox3" runat="server" placeholder="Institute ID" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text="Email" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox4" runat="server" placeholder="Email" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label7" runat="server" Text="Password" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox7" runat="server" placeholder="Password" CssClass="form-control"
                                        TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label8" runat="server" Text="Repeat Password" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox8" runat="server" placeholder="Repeat Password" CssClass="form-control"
                                        TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text="Mobile NO" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox9" runat="server" placeholder="Mobile Number" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <asp:Button ID="Button8" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Button8_Click" />
                                    <asp:Button ID="Button9" runat="server" CssClass="btn btn-warning" Text="Cancel" OnClick="Button9_Click" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>

        </asp:Panel>
        <!-------------End of SignUp lecture Content------------>

        <!-------------End of SignUp Student Content------------>
        <asp:Panel ID="PanelSignUpStud" runat="server">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="registrationform">
                    <div class="form-horizontal">
                        <fieldset>
                            <legend>Manage Groups <i class="fa fa-pencil pull-right"></i></legend>
                            <table class="table" style="color: black">
                                <tr>
                                    <th class="warning">M.I.M Kumararathna</th>
                                    <th class="success col-lg-2">Delete</th>
                                </tr>
                                <tr>
                                    <td class="warning">G.K.D Gamage</td>
                                    <td class="success col-lg-2">
                                        <asp:Button ID="Button19" runat="server" CssClass="btn-danger" Text="Delete" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="warning">B.V Fernando</td>
                                    <td class="success col-lg-2">
                                        <asp:Button ID="Button20" runat="server" CssClass="btn-danger" Text="Delete" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="warning">N.C Weerasinghe</td>
                                    <td class="success col-lg-2">
                                        <asp:Button ID="Button21" runat="server" CssClass="btn-danger" Text="Delete" />
                                    </td>
                                </tr>                
                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="registrationform">
                    <div class="form-horizontal">
                        <fieldset>
                            <legend>Register Students <i class="fa fa-pencil pull-right"></i></legend>

                            <div class="form-group">
                                <asp:Label ID="Label10" runat="server" Text="Full Name" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox10" runat="server" placeholder="Full Name" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label11" runat="server" Text="Email" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox11" runat="server" placeholder="Email" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label12" runat="server" Text="Password" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox12" runat="server" placeholder="Password" CssClass="form-control"
                                        TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label13" runat="server" Text="Repeat Password" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox13" runat="server" placeholder="Repeat Password" CssClass="form-control"
                                        TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label14" runat="server" Text="Mobile NO" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:TextBox ID="TextBox14" runat="server" placeholder="Mobile Number" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label15" runat="server" Text="Groupe" CssClass="col-lg-2 control-label"></asp:Label>
                                <div class="col-lg-10">
                                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control ddl">
                                        <asp:ListItem>13.1</asp:ListItem>
                                        <asp:ListItem>13.2</asp:ListItem>
                                        <asp:ListItem>14.1</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <asp:Button ID="Button17" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Button17_Click" />
                                    <asp:Button ID="Button18" runat="server" CssClass="btn btn-warning" Text="Cancel" OnClick="Button18_Click" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>

        </asp:Panel>
        <!-------------End of SignUp Student Content------------>

    </div>
</asp:Content>
