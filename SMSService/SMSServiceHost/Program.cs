﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SMSServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(SMSService.SMSService)))
            {
                host.Open();
                Console.WriteLine("SMS Service Started...");
                Console.ReadLine();
            }
        }
    }
}
