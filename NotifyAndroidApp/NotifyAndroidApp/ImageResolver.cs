using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NotifyAndroidApp
{
    public static class ImageResolver
    {
        public static int getGroupImage(int gId)
        {
            switch (gId)
            {
                case 2:
                    return Resource.Drawable.api;
                case 3:
                    return Resource.Drawable.computing_project;
                case 4:
                    return Resource.Drawable.database;
                case 5:
                    return Resource.Drawable.destributed_system;
                case 6:
                    return Resource.Drawable.network_monitoring;
                default:
                    return Resource.Drawable.api;
            }
        }

        public static int getGroupImage(string name)
        {
            if(name.Equals("APIDevelopment"))
            {
                return Resource.Drawable.api;
            }
            else if(name.Equals("ComputingProject"))
            {
                return Resource.Drawable.computing_project;
            }
            else if(name.Equals("DatabaseManagement"))
            {
                return Resource.Drawable.database;
            }
            else if (name.Equals("DistributedSystem"))
            {
                return Resource.Drawable.destributed_system;
            }
            else if (name.Equals("NetworkMonitoring"))
            {
                return Resource.Drawable.network_monitoring;
            }
            else
            {
                return Resource.Drawable.api;
            }
        }
    }
}