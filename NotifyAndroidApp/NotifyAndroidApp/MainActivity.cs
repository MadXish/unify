using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Android.Support.V4.Widget;
using SupportFragment = Android.Support.V4.App.Fragment;
using SupportFragmentManager = Android.Support.V4.App.FragmentManager;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using SupportActionBar = Android.Support.V7.App.ActionBar;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.App;
using NotifyAndroidApp.Fragments;
using System.ServiceModel;
using NotifyDatabaseService;
using Android.Media;
using Message = NotifyDatabaseService.Message;

namespace NotifyAndroidApp
{
    [Activity(Label = "Notify", Theme = "@style/Theme.DesignDemo")]
    public class MainActivity : AppCompatActivity
    {
        
        private DrawerLayout mDrawerLayout;
        private ViewPager vPager;
        private UnifyServiceClient _client;
        public static User logedUser;
        private bool running;

        protected override void OnPause()
        {
            running = false;
            base.OnPause();
        }

        protected override void OnResume()
        {
            running = true;
            base.OnResume();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);

            _client = ServiceHelper.InitializeHelloWorldServiceClient();

            running = true;

            
            SupportToolbar toolBar = FindViewById<SupportToolbar>(Resource.Id.toolBar);

            SetSupportActionBar(toolBar);

            SupportActionBar ab = SupportActionBar;
            ab.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);
            ab.SetDisplayHomeAsUpEnabled(true);

            mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            NavigationView navigarionView = FindViewById<NavigationView>(Resource.Id.nav_view);
            if(navigarionView != null)
            {
                SetUpDrawerContent(navigarionView);
            }

            TabLayout tabs = FindViewById<TabLayout>(Resource.Id.tabs);

            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.viewpager);

            SetUpViewPager(viewPager);

            tabs.SetupWithViewPager(viewPager);

            vPager = viewPager;

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);

            fab.Click += (o, e) =>
            {
                View anchor = o as View;

                Snackbar.Make(anchor, "Powerd By GamageSoft", Snackbar.LengthLong)
                        .SetAction("", v =>
                        {
                            //do somthing here
                            //Intent intent = new Intent();
                        })
                        .Show();
            };

            

        }

        private void NotifyService_MsgReceived(NotifyDatabaseService.Message msg)
        {
            
            if(!running)
            {
                Notification.Builder builder = new Notification.Builder(this)
                                                .SetContentTitle(msg.GroupName)
                                                .SetContentText(msg.Content)
                                                .SetSmallIcon(Android.Resource.Drawable.SymCallIncoming);

                Notification notification = builder.Build();
                
                var nMgr = GetSystemService(Context.NotificationService) as NotificationManager;

                notification.Sound = RingtoneManager.GetDefaultUri(RingtoneType.Notification);
                notification.Flags = NotificationFlags.AutoCancel | NotificationFlags.ShowLights;

                const int notificationId = 0;
                nMgr.Notify(notificationId, notification);
            }
        }

        private void SetUpViewPager(ViewPager viewPager)
        {
            TabAdapter adapter = new TabAdapter(SupportFragmentManager);
            if(logedUser.Type == NotifyDatabaseService.UserType.Lecturer)
            {
                adapter.AddFragment(new Fragment1(), "Groups");
                adapter.AddFragment(new Fragment2(), "Sent Messages");
            }
            else
            {
                adapter.AddFragment(new Fragment3(), "Received Messagess");
                // f.addMsg(new Message() { Content = "contetnt", GroupName = "APIDevelopment", ID = 1 });

                Intent intent = new Intent(this, typeof(NotifyService));
                intent.PutExtra("userId", logedUser.Id);
                StartService(intent);
                NotifyService.MsgReceived += NotifyService_MsgReceived;
            }
            //adapter.AddFragment(new Fragment3(), "Fragment 3");

            viewPager.Adapter = adapter;
            
            
        }

        //when select item inside actionbar (this app toolbar)
        //if item is hot home(hamburger) item just run base
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    mDrawerLayout.OpenDrawer((int)GravityFlags.Left);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        private void SetUpDrawerContent(NavigationView navigationView)
        {
            navigationView.NavigationItemSelected += (object sender, NavigationView.NavigationItemSelectedEventArgs e) =>
            {
                if (e.MenuItem.ItemId == Resource.Id.nav_groups)
                {
                    vPager.SetCurrentItem(0, true);
                }
                if (e.MenuItem.ItemId == Resource.Id.nav_notification)
                {
                    vPager.SetCurrentItem(1, true);
                }
                e.MenuItem.SetChecked(true);
                mDrawerLayout.CloseDrawers();
            };
        }
        

        public class TabAdapter : FragmentPagerAdapter
        {
            public List<SupportFragment> Fragments { get; set; }
            public List<string> FragmentNames { get; set; }

            public TabAdapter(SupportFragmentManager sfm) : base(sfm)
            {
                Fragments = new List<SupportFragment>();
                FragmentNames = new List<string>();
            }

            public void AddFragment(SupportFragment fragment, string name)
            {
                Fragments.Add(fragment);
                FragmentNames.Add(name);
            }

            public override int Count
            {
                get
                {
                    return Fragments.Count;
                }
            }

            public override SupportFragment GetItem(int position)
            {
                return Fragments[position];
            }

            public override ICharSequence GetPageTitleFormatted(int position)
            {
                return new Java.Lang.String(FragmentNames[position]);
            }
        }

    }
}