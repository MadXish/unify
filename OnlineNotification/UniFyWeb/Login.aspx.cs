﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UniFyWeb
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            CheckUserFromServer();
        }

        public void CheckUserFromServer()
        {  
            UniFyDBService.DatabaseServiceClient client = new UniFyDBService.DatabaseServiceClient("BasicHttpBinding_IDatabaseService");
            try
            {
                UniFyDBService.User user = client.getUserByUserName(TextBox1.Text);
                try
                {
                    if (user != null)
                    {
                        if(user.Pass == TextBox2.Text)
                        {
                            if (user.Type == UniFyDBService.UserType.Lecturer)
                            {
                                Session["new"] = user;
                                Response.Redirect("LecUserProfile.aspx");
                            }
                            else if (user.Type == UniFyDBService.UserType.Admin)
                            {
                                Session["new"] = user;
                                Response.Redirect("Admin_Dashboard.aspx");
                            }
                            else if (user.Type == UniFyDBService.UserType.Student)
                            {
                                Session["new"] = user;
                                Response.Redirect("StudentUserProfile.aspx");
                            }
                            else
                            {
                                LabelStatus.Text = "Somthing Wrong! Pleace Try Again Later";
                            }
                        }
                        else
                        {
                            LabelStatus.Text = "Username and password not match..!";
                        } 
                    }
                }
                catch(Exception)
                {
                    LabelStatus.Text = "Somthing Wrong! Pleace Try Again Later";
                } 
            }
            catch (Exception)
            {
                LabelStatus.Text = "Somthing Wrong! Pleace Try Again Later";
            }
        }
    }
}