﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SMSService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISMSService" in both code and config file together.
    [ServiceContract]
    public interface ISMSService
    {
        [OperationContract]
        void sendMessages(Message msg);

        [OperationContract]
        void deliverMesssage(int id, string message);

        [OperationContract]
        void reciveSMS();

        [OperationContract]
        void deliverMsgFromAndroidService(List<int> userIDs, DatabaseServiceConsumer.DatabaseService.Message message);
    }
}
