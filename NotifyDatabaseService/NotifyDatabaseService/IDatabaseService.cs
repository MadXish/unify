﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NotifyDatabaseService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDatabaseService" in both code and config file together.
    [ServiceContract]
    public interface IDatabaseService
    {
        [OperationContract]
        User getUserByPNo(string pNo);

        [OperationContract]
        User getUserByUserName(string userName);

        [OperationContract]
        User getUserByUserId(int userId);

        [OperationContract]
        List<Group> getGroupsByUserId(int id);

        [OperationContract]
        int saveMessage(int senderId, int groupId, Message msg);

        [OperationContract]
        void sendMessage(int receiverId, int msgId, DeliveryStatus status);

        [OperationContract]
        void deliverMessage(int receiverId, int msgId, DeliveryStatus status);

        [OperationContract]
        Group getGroupByGroupName(string groupName);

        [OperationContract]
        int getPhoneNumberByUserId(int userId);

        [OperationContract]
        void updateUser(string userId, User user);

        [OperationContract]
        void deleteUser(string userId);

        [OperationContract]
        void createGroup(Group group);

        [OperationContract]
        void createUser(User user);

        [OperationContract]
        void assignUserToGroup(List<int> userIds, int groupId);

        [OperationContract]
        void unassignUserFromGroup(List<int> userIds, int groupId);

        [OperationContract]
        void updateGroup(int groupId, Group group);

        [OperationContract]
        void deleteGroup(int groupId);

        [OperationContract]
        List<User> getUsersOfGroup(int groupId);

        [OperationContract]
        List<Message> getMessagesOfUser(int userId);

        [OperationContract]
        List<Message> getFewMessages(int userId);

        [OperationContract]
        List<Message> getSendMessagesBySenderId(int userId);

        [OperationContract]
        Group getGroupByAtName(string atName);

        [OperationContract]
        Group getGroupByGroupid(int groupId);

        [OperationContract]
        User testMethod(string atName);
    }
}
