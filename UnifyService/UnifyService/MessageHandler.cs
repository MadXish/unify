﻿using DatabaseServiceConsumer.DatabaseService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnifyService
{
    public class MessageHandler
    {
        private static Message holdMsg;
        private static object holdMsgLock = new object();
        private static List<int> resUsers = new List<int>();

        public static void publishMsg(List<int> users, Message msg)
        {
            lock (holdMsgLock)
            {
                holdMsg = msg;
                resUsers = users;
            }
        }

        public static Message checkMsg(int userId)
        {
            lock (holdMsgLock)
            {
                if (resUsers.Contains(userId))
                {
                    resUsers.Remove(userId);
                    return holdMsg;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<int> getRestReviceivers()
        {
            List<int> restReceivers;
            lock (holdMsgLock)
            {
                Console.WriteLine("msg contetnt "+holdMsg.Content);
                Console.WriteLine("host list count -"+resUsers.Count);
                restReceivers = resUsers;
                //resUsers.Clear();
            }
            return restReceivers;
        }
        

    }
}
