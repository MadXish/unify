﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DatabaseServiceConsumer.DatabaseService;
using DatabaseServiceConsumer;
using System.Threading.Tasks;
using System.Threading;

namespace UnifyService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UnifyService" in both code and config file together.
    public class UnifyService : IUnifyService
    {
        private Dao dao = new Dao();
        private SMSServiceConsumer.SMSDao smsDao = new SMSServiceConsumer.SMSDao();

        public User login(string userName, string password)
        {
            Console.WriteLine("Request come to login ");
            User user = dao.getUserByUserName(userName);
            if(user != null && user.Pass.Equals(password))
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public List<Message> getLastMessages(int userId)
        {
            return dao.getLastMessages(userId);
        }

        public List<Group> getGroups(int userId)
        {
            List<Group> groups = dao.getGroupsByUserId(userId);
            Console.WriteLine("group count - "+groups.Count);
            return groups;
        }

        public Message checkMsg(int userId)
        {
            return MessageHandler.checkMsg(userId);
        }

        public void sendMsg(int groupId, int senderId, Message msg)
        {
            Console.WriteLine("group id------"+groupId);
            dao.sendMessage(groupId, senderId, msg);
            Task.Run(() => publishMsg(groupId,msg));

        }

        private List<int> usersToUserIds(List<User> users)
        {
            List<int> userIds = new List<int>();
            foreach(User user in users)
            {
                userIds.Add(user.Id);
            }
            return userIds;
        }

        private void publishMsg(int groupId,Message msg)
        {
            List<int> userIds = usersToUserIds(dao.getUsersOfGroup(groupId));
            Console.WriteLine("lsit count +"+userIds.Count);
            MessageHandler.publishMsg(userIds, msg);
            Thread.Sleep(30000);
            Console.WriteLine("out from sleep....");
            List<int> restReceivers = MessageHandler.getRestReviceivers();
            smsDao.SendMsgToDelivery(restReceivers, msg);
        }

        public void sendMsgFromWeb(int groupId, Message msg)
        {
            Console.WriteLine("msg received from web.........."+groupId+" mess"+msg.Content);
            Task.Run(() => publishMsg(groupId, msg));
        }
    }
}
