﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LecUserProfile.aspx.cs" Inherits="UniFyWeb.LecUserProfile" MasterPageFile="~/Index.Master" %>

<asp:Content ID="contentSendMsg" ContentPlaceHolderID="contentHolder" runat="server">

    <div class="container">
        <div class="row">
            <h3>My Account</h3>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-3" style="background-color: rgb(0,174,239);">
                <div class="row" style="height: 250px;">
                    <img src="img/profilePic.jpg" style="height: 250px; width: 100%; background-repeat: no-repeat; background-size: cover" />
                </div>
                <aside style="margin-top: 20px; padding-bottom: 20px;">
        	
            <ul class="nav nav-pills nav-stacked">
                <li><asp:LinkButton ID="LinkButtonSendMsg" runat="server" OnClick="LinkButtonSendMsg_Click"><font color="black">Send Message</font></asp:LinkButton></li>
                <li><asp:LinkButton ID="LinkButtonProfile" runat="server" OnClick="LinkButtonProfile_Click"><font color="black">Profile</font></asp:LinkButton></li>
                <li><asp:LinkButton ID="LinkButtonEditProfile" runat="server" OnClick="LinkButtonEditProfile_Click"><font color="black">Edit Profile</font></asp:LinkButton></li>  
            </ul> 
         </aside>
            </div>
            <asp:Panel ID="PanelProfile" runat="server">
                <div class="col-md-9">
                    <div class="row">

                        <table class="auto-style1 table table-condensed" style="color: black">
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">First Name</td>
                                <td class="active">
                                    <asp:Label ID="lblFName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Email</td>
                                <td class="active">
                                    <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Address</td>
                                <td class="active">
                                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">DOB</td>
                                <td class="active" style="width: 497px">
                                    <asp:Label ID="lblDOB" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Tel</td>
                                <td class="active">
                                    <asp:Label ID="lblTel" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Gender</td>
                                <td class="active">
                                    <asp:Label ID="lblGender" runat="server"></asp:Label>
                                </td>
                            </tr>

                        </table>

                        <ul class="nav nav-pills" role="tablist" style="margin-top: -23px; background-color: white">
                            <%--<li role="presentation" class="active"><a href="#">New Messages <span class="badge">42</span></a></li>--%>
                            <li role="presentation"><a href="#">Inbox</a></li>
                        </ul>

                    </div>

                    <!------Notifications------>
                    <div class="row">

                        <asp:Table ID="msgTable" CssClass="table" runat="server">

                        </asp:Table>

                        <%--<table class="table" style="color: black">
                            <tr>
                                <th class="warning">Message</th>
                                <th class="success col-lg-2">Date</th>
                            </tr>
                            <tr>
                                <td class="warning">Exam on this weekend</td>
                                <td class="success col-lg-2">2016/04/22</td>
                            </tr>
                            <tr>
                                <td class="warning">Exam on this weekend</td>
                                <td class="success col-lg-2">2016/04/22</td>
                            </tr>
                            <tr>
                                <td class="warning">Exam on this weekend</td>
                                <td class="success col-lg-2">2016/04/22</td>
                            </tr>
                            <tr>
                                <td class="warning">Exam on this weekend</td>
                                <td class="success col-lg-2">2016/04/22</td>
                            </tr>
                            <tr>
                                <td class="warning">Exam on this weekend</td>
                                <td class="success col-lg-2">2016/04/22</td>
                            </tr>
                            <tr>
                                <td class="warning">Exam on this weekend</td>
                                <td class="success col-lg-2">2016/04/22</td>
                            </tr>
                            <tr>
                                <td class="warning">Exam on this weekend</td>
                                <td class="success col-lg-2">2016/04/22</td>
                            </tr>
                        </table>--%>

                    </div>

                </div>
            </asp:Panel>

            <!------Send Message Content------>
            <asp:Panel ID="PanelSendMsg" runat="server">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="registrationform">
                        <div class="form-horizontal">
                            <fieldset>
                                <legend>Send Message <i class="fa fa-pencil pull-right"></i></legend>

                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" Text="Message" CssClass="col-lg-2 control-label"></asp:Label>
                                    <div class="col-lg-10">
                                        <asp:TextBox ID="TextBox7" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="Select Group" CssClass="col-lg-2 control-label"></asp:Label>
                                    <div class="col-lg-10">
                                        <asp:DropDownList ID="DropDownGroups" runat="server" CssClass="form-control ddl" AutoPostBack="true">
                                            <asp:ListItem Value="0">---Select----</asp:ListItem>
                                            <asp:ListItem Value="1">13.1 Plymouth</asp:ListItem>
                                            <asp:ListItem Value="2">13.2 Plymouth</asp:ListItem>
                                            <asp:ListItem Value="3">14.2 Plymouth</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <asp:Button ID="btnSend" runat="server" CssClass="btn btn-primary" Text="Send" OnClick="btnSend_Click" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-warning" Text="Cancel" />
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!------End of Send Message Content------>

            <!------Edit Profile Content------>
            <asp:Panel ID="PanelEitProfile" runat="server">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                    <div class="registrationform">
                        <div class="form-horizontal">
                            <fieldset>
                                <legend>Edit Profile Picture <i class="fa fa-pencil pull-right"></i></legend>
                                <div class="form-group">
                                    <img src="img/profilePic.jpg" class="img-rounded img-responsive" style="height: 250px; width: 250px; background-size: cover; background-repeat: no-repeat" />
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-10">
                                        <asp:Button ID="Button2" runat="server" CssClass="btn btn-primary" Text="Upload" />
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                    <div class="registrationform">
                        <div class="form-horizontal">
                            <fieldset>
                                <legend>Edit Login Credintials <i class="fa fa-pencil pull-right"></i></legend>
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="Old Password" CssClass="col-lg-2 control-label"></asp:Label>
                                    <div class="col-lg-10">
                                        <asp:TextBox ID="TextBox1" runat="server" placeholder="Old Passwor" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="New Password" CssClass="col-lg-2 control-label"></asp:Label>
                                    <div class="col-lg-10">
                                        <asp:TextBox ID="TextBox2" runat="server" placeholder="New Password" CssClass="form-control"
                                            TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="Repeat Password" CssClass="col-lg-2 control-label"></asp:Label>
                                    <div class="col-lg-10">
                                        <asp:TextBox ID="TextBox3" runat="server" placeholder="Repeat New Password" CssClass="form-control"
                                            TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="LabelStatus" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!------End of End Profile Content------>

        </div>

    </div>
</asp:Content>