﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UniFyWeb
{
    public partial class LecUserProfile : System.Web.UI.Page
    {
        public List<UniFyDBService.Group> Groups = new List<UniFyDBService.Group>();
        public List<UniFyDBService.User> Users = new List<UniFyDBService.User>();
        public List<UniFyDBService.Message> Messages = new List<UniFyDBService.Message>();
        UniFyDBService.DatabaseServiceClient client = new UniFyDBService.DatabaseServiceClient();
        UniFySMSService.SMSServiceClient clientSMS = new UniFySMSService.SMSServiceClient();
        UniFyAndroidService.UnifyServiceClient androidClient = new UniFyAndroidService.UnifyServiceClient();
        UniFyDBService.User newUser = new UniFyDBService.User();
        UniFyDBService.Message msg = new UniFyDBService.Message();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bringId();
            }

            if (Session["new"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                PanelSendMsg.Visible = false;
                PanelEitProfile.Visible = false;
                PanelProfile.Visible = true;

                newUser = (UniFyDBService.User)Session["new"];
                Messages = client.getMessagesOfUser(newUser.Id);

                TableHeaderRow hrow = new TableHeaderRow();
                TableCell cellh1 = new TableCell();
                cellh1.Text = "Message";
                cellh1.Style["color"] = "black";
                cellh1.Style["font-size"] = "medium";
                cellh1.Style["font-weight"] = "bold";
                cellh1.Attributes.Add("class", "warning");
                hrow.Cells.Add(cellh1);
                msgTable.Rows.Add(hrow);

                foreach (UniFyDBService.Message msg in Messages)
                {
                    TableRow row = new TableRow();
                    TableCell cell1 = new TableCell();

                    cell1.Text = msg.Content;
                    cell1.Style["color"] = "black";
                    cell1.Attributes.Add("class", "warning");

                    row.Cells.Add(cell1);
                    msgTable.Rows.Add(row);
                }
            }
        }

        private void bringId()
        {
            newUser = (UniFyDBService.User)Session["new"];
            lblFName.Text = newUser.Name.ToString();
            lblTel.Text = newUser.PNo.ToString();
            lblAddress.Text = "Colombo";
            lblDOB.Text = "1992/10/11/";
            lblGender.Text = "Male";
            lblEmail.Text = "isharazone@gmail.com";
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            msg.Content = TextBox7.Text;
            //Save to Database
            newUser = (UniFyDBService.User)Session["new"];
            int msgId = client.saveMessage(newUser.Id, Convert.ToInt32(DropDownGroups.SelectedItem.Value),msg);

            Users = client.getUsersOfGroup(Convert.ToInt32(DropDownGroups.SelectedItem.Value));
            UniFyDBService.Group selectedgroup = client.getGroupByGroupid(Convert.ToInt32(DropDownGroups.SelectedItem.Value));
            msg.GroupName = selectedgroup.GroupName;

            //try to send android service
            try
            {
                UniFyAndroidService.Message messageAn = new UniFyAndroidService.Message();
                messageAn.GroupName = selectedgroup.GroupName;
                messageAn.Content = msg.Content;
                messageAn.ID = msgId;
                androidClient.sendMsgFromWeb(Convert.ToInt32(DropDownGroups.SelectedItem.Value), messageAn);
            }
            catch(Exception)
            {
                try
                {
                    foreach (UniFyDBService.User user in Users)
                    {
                        //client.sendMessage(user.Id, msgId, UniFyDBService.DeliveryStatus.NotDelivered);

                        //try to send SMS service
                        clientSMS.deliverMesssage(user.Id, msg.Content);
                    }

                    if (Session != null)
                    { Response.Redirect("LecUserProfile.aspx"); }
                    else
                    { Response.Redirect("Login.aspx"); }
                }
                catch(Exception)
                {
                    Console.WriteLine("Cannot Reach Any Service");
                }
            }

            
        }

        private void Print(string s)
        {
            Console.WriteLine(s);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }

        protected void LinkButtonEditProfile_Click(object sender, EventArgs e)
        {
            PanelEitProfile.Visible = true;
            PanelProfile.Visible = false;
            PanelSendMsg.Visible = false;
        }

        protected void LinkButtonSendMsg_Click(object sender, EventArgs e)
        {
            PanelProfile.Visible = false;
            PanelSendMsg.Visible = true;
            PanelEitProfile.Visible = false;

            newUser = (UniFyDBService.User)Session["new"];
            Groups = client.getGroupsByUserId(newUser.Id);
            DropDownGroups.DataTextField = "GroupName";
            DropDownGroups.DataValueField = "Id";
            DropDownGroups.DataSource = Groups;
            DropDownGroups.DataBind();
        }

        protected void LinkButtonProfile_Click(object sender, EventArgs e)
        {
            PanelProfile.Visible = true;
            PanelEitProfile.Visible = false;
            PanelSendMsg.Visible = false;
        }
    }
}