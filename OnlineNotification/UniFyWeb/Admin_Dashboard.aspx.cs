﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UniFyWeb
{
    public partial class Admin_Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["new"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                PanelAddGroup.Visible = true;
                PanelSignUpLec.Visible = false;
                PanelSignUpStud.Visible = false;
            }
        }

        protected void LinkButtonAddGroup_Click(object sender, EventArgs e)
        {
            PanelAddGroup.Visible = true;
        }

        protected void LinkButtonAddLecture_Click(object sender, EventArgs e)
        {
            PanelSignUpLec.Visible = true;
        }

        protected void LinkButtonAddStudents_Click(object sender, EventArgs e)
        {
            PanelSignUpStud.Visible = true;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void Button8_Click(object sender, EventArgs e)
        {

        }

        protected void Button9_Click(object sender, EventArgs e)
        {

        }

        protected void Button17_Click(object sender, EventArgs e)
        {

        }

        protected void Button18_Click(object sender, EventArgs e)
        {

        }
    }
}