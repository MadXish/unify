﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentUserProfile.aspx.cs" Inherits="UniFyWeb.StudentUserProfile" MasterPageFile="~/Index.Master" %>

<asp:Content ID="contentProfile" ContentPlaceHolderID="contentHolder" runat="server">
    <div class="container">
        <div class="row">
            <h3>My Account</h3>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-3" style="background-color: rgb(0,174,239);">
                <div class="row" style="height: 250px;">
                    <img src="img/profilePic.jpg" style="height: 250px; width: 100%; background-repeat: no-repeat; background-size: cover" />
                </div>
                <aside style="margin-top: 20px; padding-bottom:20px;">
        	
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><asp:LinkButton ID="LinkBtnProfile" runat="server" OnClick="LinkBtnProfile_Click">Profile</asp:LinkButton></li>
                <li><asp:LinkButton ID="LinkBtnEditProfile" runat="server" OnClick="LinkBtnEditProfile_Click">Edit Profile</asp:LinkButton></li>
            </ul> 
         </aside>
            </div>
            <asp:Panel ID="PanelStudProf" runat="server">
                <div class="col-md-9">
                    <div class="row">

                        <table class="auto-style1 table table-condensed" runat="server" style="color: black">
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">First Name</td>
                                <td class="active">
                                    <asp:Label ID="lblFName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Email</td>
                                <td class="active">
                                    <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Address</td>
                                <td class="active">
                                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">DOB</td>
                                <td class="active" style="width: 497px">
                                    <asp:Label ID="lblDOB" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Tel</td>
                                <td class="active">
                                    <asp:Label ID="lblTel" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2 success" style="width: 182px; font-weight: bold; font-style: italic;">Gender</td>
                                <td class="active">
                                    <asp:Label ID="lblGender" runat="server"></asp:Label>
                                </td>
                            </tr>

                        </table>

                        <ul class="nav nav-pills" role="tablist" style="margin-top: -23px; background-color:white">
                            <li role="presentation" class="active"><a href="#">New Messages <span class="badge">42</span></a></li>
                            <li role="presentation"><a href="#">Inbox <span class="badge">3</span></a></li>
                        </ul>

                    </div>

                    <!------Notifications------>
                    <div class="row">

<%--                        <table class="table" style="color: black">
                            <tr>
                                <th class="warning">Message</th>
                                <th class="success col-lg-2"><asp:Button ID="Button1" runat="server" CssClass="btn-danger" Text="Delete" /></th>
                            </tr>
                            <tr>
                                <td class="warning">Exam on this weekend</td>
                                <td class="success col-lg-2"><asp:Button ID="Button3" runat="server" CssClass="btn-danger" Text="Delete" /></td>
                            </tr>
                            
                        </table>--%>

                        <asp:Table ID="msgTable" CssClass="table" runat="server">
<%--                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell CssClass="warning">Message</asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="success col-lg-2">Action</asp:TableHeaderCell>
                            </asp:TableHeaderRow>--%>
<%--                            <asp:TableRow runat="server">
                                <asp:TableCell CssClass="warning">Exam on this weekend</asp:TableCell>
                                <asp:TableCell CssClass="success col-lg-2"><asp:Button ID="Button4" runat="server" CssClass="btn-danger" Text="Delete" /></asp:TableCell>
                            </asp:TableRow>--%>
                        </asp:Table>

                    </div>

                </div>
            </asp:Panel>

            <!------Edit Profile Content------>
            <asp:Panel ID="PanelEditProfileStud" runat="server">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                    <div class="registrationform">
                        <div class="form-horizontal">
                            <fieldset>
                                <legend>Edit Profile Picture <i class="fa fa-pencil pull-right"></i></legend>
                                <div class="form-group">
                                    <img src="img/profilePic.jpg" class="img-rounded img-responsive" style="height: 250px; width: 250px; background-size: cover; background-repeat: no-repeat" />
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-10">
                                        <asp:Button ID="Button2" runat="server" CssClass="btn btn-primary" Text="Upload" />
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                    <div class="registrationform">
                        <div class="form-horizontal">
                            <fieldset>
                                <legend>Edit Login Credintials <i class="fa fa-pencil pull-right"></i></legend>
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="Old Password" CssClass="col-lg-2 control-label"></asp:Label>
                                    <div class="col-lg-10">
                                        <asp:TextBox ID="TextBox1" runat="server" placeholder="Old Passwor" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="New Password" CssClass="col-lg-2 control-label"></asp:Label>
                                    <div class="col-lg-10">
                                        <asp:TextBox ID="TextBox2" runat="server" placeholder="New Password" CssClass="form-control"
                                            TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="Repeat Password" CssClass="col-lg-2 control-label"></asp:Label>
                                    <div class="col-lg-10">
                                        <asp:TextBox ID="TextBox3" runat="server" placeholder="Repeat New Password" CssClass="form-control"
                                            TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="LabelStatus" runat="server" Text=""></asp:Label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <!------End of End Profile Content------>
        </div>
    </div>
</asp:Content>