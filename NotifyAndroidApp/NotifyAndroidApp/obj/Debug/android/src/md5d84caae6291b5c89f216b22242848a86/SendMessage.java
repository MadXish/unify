package md5d84caae6291b5c89f216b22242848a86;


public class SendMessage
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("NotifyAndroidApp.SendMessage, NotifyAndroidApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", SendMessage.class, __md_methods);
	}


	public SendMessage () throws java.lang.Throwable
	{
		super ();
		if (getClass () == SendMessage.class)
			mono.android.TypeManager.Activate ("NotifyAndroidApp.SendMessage, NotifyAndroidApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
