using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using System.ServiceModel;
using NotifyDatabaseService;

namespace NotifyAndroidApp
{
    [Activity(Label = "Login", MainLauncher = true, Theme = "@style/Theme.DesignDemo")]
    public class Login : Activity
    {
        Button btnLogin;
        Button btnCancel;
        TextInputLayout txtUserName;
        TextInputLayout txtPassword;
        UnifyServiceClient _client;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Login);

            txtUserName = FindViewById<TextInputLayout>(Resource.Id.txtUserName);
            txtPassword = FindViewById<TextInputLayout>(Resource.Id.txtPassword);
            btnLogin = FindViewById<Button>(Resource.Id.btnLogin);
            btnCancel = FindViewById<Button>(Resource.Id.btnCancel);
            _client = ServiceHelper.InitializeHelloWorldServiceClient();
            
            btnLogin.Click += BtnLogin_Click;
            btnCancel.Click += BtnCancel_Click;
            _client.loginCompleted += _client_loginCompleted;

        }
        

        private void _client_loginCompleted(object sender, loginCompletedEventArgs e)
        {
            User user = e.Result;
            if (user != null)
            {
                MainActivity.logedUser = user;
                Console.WriteLine("success");
                Intent intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
                Finish();
            }
            else
            {
                RunOnUiThread(() => { txtPassword.Error = "Invalid user name or password"; });
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Finish();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            string userName = txtUserName.EditText.Text;
            string pass = txtPassword.EditText.Text;
            Console.WriteLine("user details - "+userName+" - "+pass);
            _client.loginAsync(userName, pass);
        }
    }
}